# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""The "test" command"""
import http.cookiejar as cookiejar
import re
import json
import yaml
from kpet import misc, patch, data, run, ssp, cmd_misc

# Needed for yaml.dump to not create aliases for maintainers
yaml.Dumper.ignore_aliases = lambda *args: True


class YamlDumper(yaml.Dumper):  # pylint: disable=too-many-ancestors
    """"Dumper that adds an indent to all list items for yaml dump"""
    def increase_indent(self, flow=False, indentless=False):
        return super().increase_indent(flow, False)


def build(cmds_parser, common_parser):
    """Build the argument parser for the test command"""
    _, action_subparser = cmd_misc.build(
        cmds_parser,
        common_parser,
        'test',
        help='Test',
    )
    list_parser = action_subparser.add_parser(
        "list",
        help='List tests',
        parents=[common_parser],
    )
    list_parser.add_argument(
        '--domains',
        metavar='REGEX',
        help='A regular expression matching domains to restrict tests to. '
        'Only tests targeting hosts in matching domains will be listed. '
        'Not allowed, if the database has no domains defined. ' +
        'Run "kpet domain list" to see all available domains.'
    )
    list_parser.add_argument(
        '-t',
        '--trees',
        metavar='REGEX',
        help='A regular expression matching the names of kernel trees, which '
             'listed tests should match. '
             'Run "kpet tree list" to see recognized trees. '
    )
    list_parser.add_argument(
        '-a',
        '--arches',
        metavar='REGEX',
        help='A regular expression matching the names of architectures, which '
             'listed tests should match. '
             'Run "kpet arch list" to see supported architectures. '
    )
    list_parser.add_argument(
        '-c',
        '--components',
        metavar='REGEX',
        help='A regular expression matching extra components included '
             'into the kernel build, which listed tests should match. '
             'Run "kpet component list" to see recognized components.'
    )
    list_parser.add_argument(
        '-s',
        '--sets',
        metavar='PATTERN',
        help='Test set pattern: regexes (fully) matching names of test sets, '
        'that listed tests should belong to, combined using &, |, !, and () '
        'operators, which can be escaped with \\. Run "kpet set list" to see '
        'available sets.'
    )
    list_parser.add_argument(
        '--tests',
        action='append',
        metavar='REGEX',
        help='A regular expression (fully) matching names of tests to list.'
    )
    list_parser.add_argument(
        '--all-sources',
        action='store_true',
        help='Indicate that potentially all source files have changed, '
             'listing tests matching any changed sources, and not requiring '
             'specific changes. Cannot be used with MBOX\'es.'
    )
    list_parser.add_argument(
        '-o',
        '--output',
        default='text',
        choices=['yaml', 'json', 'text'],
        help='Change output format, "json" and "yaml" give more information.'
    )
    list_parser.add_argument(
        '--cookies',
        metavar='FILE',
        default=None,
        help='Cookies to send when downloading patches, Netscape-format file.'
    )
    list_parser.add_argument(
        'mboxes',
        metavar='MBOX',
        nargs='*',
        default=[],
        help='URL/path of a patch mailbox changing files '
             'which listed tests should match. If none are specified, '
             'no assumption is made whether, or which, files were changed, '
             'and all source file conditions of listed tests are ignored. '
             'Cannot be specified with --all-sources.'
    )


# pylint: disable=too-many-branches
def main_create_scenario(args, database):
    """
    Create a scenario for specified test database and command-line arguments.

    Args:
        args:       Parsed command-line arguments.
        database:   The database to create a scenario for.

    Returns:
        The created scenario.
    """
    trees_target_sets = [data.Target.UNKNOWN]
    arches_target_sets = [data.Target.UNKNOWN]
    components_target_set = data.Target.UNKNOWN
    sources_target_set = data.Target.UNKNOWN
    match_sets = None

    cookies = cookiejar.MozillaCookieJar()
    if args.cookies:
        cookies.load(args.cookies)
    if args.all_sources:
        sources_target_set = data.Target.ALL
        if args.mboxes:
            raise Exception("Both MBOX'es and --all-sources are specified.")
    elif args.mboxes:
        sources_target_set = patch.get_src_set_from_location_set(
            set(args.mboxes), cookies
        )
    if database.domains is None:
        if args.domains is not None:
            raise Exception("Database has no domains specified, "
                            "but the --domains option is provided")
        domains = {None}
    else:
        domains = set(database.domains)
        if args.domains is not None:
            domain_regex = re.compile(args.domains)
            domains = list(filter(domain_regex.fullmatch, domains))
            if not domains:
                raise Exception(f"Regular expression {args.domains!r} "
                                f"matches no domains")
    if args.trees:
        trees_target_sets = [{x} for x in database.trees
                             if re.fullmatch(args.trees, x)]
        if database.trees and not trees_target_sets:
            raise Exception("No trees matched specified regular " +
                            "expression: {}".format(args.trees))
    if args.arches:
        arches_target_sets = [{x} for x in database.arches
                              if re.fullmatch(args.arches, x)]
        if database.arches and not arches_target_sets:
            raise Exception("No architectures matched specified regular " +
                            "expression: {}".format(args.arches))
    if args.components:
        components_target_set = set(x for x in database.components
                                    if re.fullmatch(args.components, x))
        if database.components and not components_target_set:
            raise Exception("No components matched specified regular " +
                            "expression: {}".format(args.arches))
    if args.sets is not None:
        try:
            match_sets = ssp.parse(args.sets, set(database.sets))
        except (ssp.InvalidCharacter,
                ssp.InvalidSyntax,
                ssp.VoidRegex) as exc:
            raise Exception(
                f"Failed parsing set pattern: {repr(args.sets)}"
            ) from exc

    scenario = run.Scenario(database)
    for domain in domains:
        for trees_target_set in trees_target_sets:
            for arches_target_set in arches_target_sets:
                scenario.add_scene(
                    domain=domain,
                    target=data.Target(arches=arches_target_set,
                                       trees=trees_target_set,
                                       components=components_target_set,
                                       sources=sources_target_set),
                    match_sets=match_sets,
                    test_regexes=args.tests
                )
    return scenario


# pylint: disable=unused-argument
def main_list(scenario, output_type):
    """
    Execute `test list`

    Args:
        scenario:    A scenario to list tests from.
        output_type: Type of the printed output.
    """
    output_data = []
    for test in scenario.get_tests():
        test_dict = {}
        test_dict["name"] = test.name
        if test.universal_id:
            test_dict["universal_id"] = test.universal_id
        test_dict["maintainers"] = test.maintainers
        output_data.append(test_dict)
    output_data = sorted(output_data, key=lambda i: i["name"])

    if output_type == "text":
        for test in output_data:
            print(test["name"])
    elif output_type == "yaml":
        print(yaml.dump(output_data, Dumper=YamlDumper,
                        default_flow_style=False, sort_keys=False), end="")
    elif output_type == "json":
        print(json.dumps(output_data, indent=4))


def main(args):
    """Main function for the `test` command"""
    if not data.Base.is_dir_valid(args.db):
        misc.raise_invalid_database(args.db)
    database = data.Base(args.db)

    if args.action == 'list':
        main_list(main_create_scenario(args, database), args.output)
    else:
        misc.raise_action_not_found(args.action, args.command)
